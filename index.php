    <?php
    /**
     * Файл примера правильного документирования
     *
     * Здесь я постараюсь использовать
     * максимально много тегов
     * @author Baryshev <baryshev1983@yandex.ru>
     * @version 1.0
     */

    /**
     * Автолоадер composer-а (подгружает ZendDb)
     */
    include 'vendor/autoload.php';

    use Zend\Db\Adapter\Adapter;
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Sql\Sql;
    use Zend\Db\Sql\Ddl\CreateTable;
    use Zend\Db\Sql\Ddl\Column;
    use Zend\Db\Sql\Ddl\Constraint;

    final class init {

        /**
         * Набор параметрав определяющих СУРБД и доступ к ней
         * @var array
         */
        const DB_PARAMS = [
            'driver' => 'Mysqli',
            'database' => 'bj',
            'username' => 'root',
            'password' => ''
        ];

        /** Количество генерируемых рандомных данных
         * @var int
         */
        const COUNT_RANDOM_FIELD = 10;

        /** Тип результата работы скрипта
         * @var array
         */
        const TYPE_SCRIPT_RESULTS = [
            'normal',
            'illegal',
            'failed',
            'success'
        ];

        /** Адаптер доступа к БД
         * @var null|Adapter
         */
        private $adapter = null;

        /**
         * В конструкторе класса создеётся таблица и заполняется рандомными значениями
         * @return void
         */
        function __construct()
        {
            $this->adapter = new Adapter(self::DB_PARAMS);

            $this->create();
            $this->fill();
        }

        /**
         * Создаёт таблицу
         * @return void
         */
        private function create() :void
        {
            $createTable = new CreateTable('test');
            $sql = new Sql($this->adapter);

            $createTable->addColumn(new Column\Integer('id', false, null, ['autoincrement' => true]));
            $createTable->addColumn(new Column\VarChar('script_name', 25));
            $createTable->addColumn(new Column\Integer('start_time'));
            $createTable->addColumn(new Column\Integer('end_time'));
            $createTable->addColumn(new Column\VarChar('result', 7));
            $createTable->addConstraint(new Constraint\PrimaryKey('id'));
            try {
                $this->adapter->query(
                    $sql->buildSqlString($createTable),
                    $this->adapter::QUERY_MODE_EXECUTE
                );
            } catch (Exception $exception) {
                echo $exception->getMessage() . PHP_EOL;
            }

        }

        /**
         * Заполняет таблицу рандомными записями
         * @return void
         */
        private function fill() :void
        {
            $testTable = new TableGateway('test', $this->adapter);
            $now = (new DateTime())->getTimestamp();

            for ($i = 1; $i <= self::COUNT_RANDOM_FIELD; $i++) {
                $testTable->insert([
                    'script_name' => '/home/scripts/' . $i . '.php',
                    'start_time' => $now,
                    'end_time' => $now + $i,
                    'result' => self::TYPE_SCRIPT_RESULTS[mt_rand(0,count(self::TYPE_SCRIPT_RESULTS) - 1)],
                ]);
            }
        }

        /**
         * возвращает массив записей с результатамим normal, success
         * @return array
         */
        public function get() :array
        {
            $testTable = new TableGateway('test', $this->adapter);

            return $testTable->select([
                'result' => [
                    'normal',
                    'success'
                ]
            ])->toArray();

        }
    }

    $init = new init();
    $selectedRecords = $init->get();

    foreach ($selectedRecords as $record) {
        echo "|\t" . implode("\t|\t", $record) . "\t|" . PHP_EOL;
    }
